<!-- Footer -->
<footer class="main">
	&copy; <?php echo date(Y); ?> <strong>Oline IT</strong>. 
    Developed by 
	<a href="http://olineit.com" 
    	target="_blank">Oline IT</a>
</footer>
