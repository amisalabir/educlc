<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sms_model extends CI_Model {
    
    public function __construct() {

                parent::__construct();
    }

    //COMMON FUNCTION FOR SENDING SMS
    function send_sms($message = '' , $reciever_phone = '')
    {
        $active_sms_service = $this->db->get_where('settings' , array(
            'type' => 'active_sms_service'
        ))->row()->description;
        if ($active_sms_service == '' || $active_sms_service == 'disabled')
            return;
        if ($active_sms_service == 'clickatell') {
            $this->send_sms_via_clickatell($message , $reciever_phone );
        }
        if ($active_sms_service == 'oline') {
            $this->send_sms_via_oline($message , $reciever_phone );
        }
        if ($active_sms_service == 'twilio') {
            $this->send_sms_via_twilio($message , $reciever_phone );
        }
    }
    
    // SEND SMS VIA CLICKATELL API
    function send_sms_via_clickatell($message = '' , $reciever_phone = '') {
        
        $clickatell_user       = $this->db->get_where('settings', array('type' => 'clickatell_user'))->row()->description;
        $clickatell_password   = $this->db->get_where('settings', array('type' => 'clickatell_password'))->row()->description;
        $clickatell_api_id     = $this->db->get_where('settings', array('type' => 'clickatell_api_id'))->row()->description;
        $clickatell_baseurl    = "http://api.clickatell.com";

        $text   = urlencode($message);
        $to     = $reciever_phone;

        // auth call
        $url = "$clickatell_baseurl/http/auth?user=$clickatell_user&password=$clickatell_password&api_id=$clickatell_api_id";

        // do auth call
        $ret = file($url);

        // explode our response. return string is on first line of the data returned
        $sess = explode(":",$ret[0]);
        print_r($sess);echo '<br>';
        if ($sess[0] == "OK") {

            $sess_id = trim($sess[1]); // remove any whitespace
            $url = "$clickatell_baseurl/http/sendmsg?session_id=$sess_id&to=$to&text=$text";

            // do sendmsg call
            $ret = file($url);
            $send = explode(":",$ret[0]);
            print_r($send);echo '<br>';
            if ($send[0] == "ID") {
                echo "successnmessage ID: ". $send[1];
            } else {
                echo "send message failed";
            }
        } else {
            echo "Authentication failure: ". $ret[0];
        }
    }

     // SEND SMS VIA Oline API
    function send_sms_via_oline($message = '' , $reciever_phone = '') {

        $user       = $this->db->get_where('settings', array('type' => 'oline_user'))->row()->description;
        $password   = $this->db->get_where('settings', array('type' => 'oline_password'))->row()->description;
        $oline_api_id     = $this->db->get_where('settings', array('type' => 'oline_api_id'))->row()->description;
        $sender     = $this->db->get_where('settings', array('type' => 'oline_sender_id'))->row()->description;
        $api_url     = $this->db->get_where('settings', array('type' => 'oline_api_url'))->row()->description;
        //$sender="8801552146120";
        $text   = urlencode($message);
        $to     = $reciever_phone;
        ##################################################################
        /*
        //$url = "http://api.mimsms.com/api/v3/sendsms/plain?user=$user&password=$password&sender=$sender&SMSText=$text&GSM=$to";
        $url = "http://esms.oline.com/smsapi?api_key=$oline_api_id&type=text&contacts=$to&senderid=$sender&msg=$text";

        $smsResult = simplexml_load_file($url);
        if($smsResult){
            var_dump($smsResult);
        }
        else {
            echo "Authentication failure: ";
        }
        */
        ###################################################################

        $post_url = 'http://portal.olineit.com/smsapi/' ;

        $post_values = array(
            'api_key' => $oline_api_id,
            'type' => 'text',  // unicode or text
            'senderid' =>  $sender,
            'contacts' => $to,
            'msg' => $text,
            'method' => 'api'
        );

        $post_string = "";
        foreach( $post_values as $key => $value )
        { $post_string .= "$key=" . urlencode( $value ) . "&"; }
        $post_string = rtrim( $post_string, "& " );

        $request = curl_init($post_url);
        curl_setopt($request, CURLOPT_HEADER, 0);
        curl_setopt($request, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($request, CURLOPT_POSTFIELDS, $post_string);
        curl_setopt($request, CURLOPT_SSL_VERIFYPEER, FALSE);
        $post_response = curl_exec($request);
        curl_close ($request);

        $responses=array();
        $array =  json_decode( preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $post_response), true );

        if($array){
            echo $array['status'] ;

            echo $array['CamID'] ;

            print_r($array);
        }
        else {
            //echo "Authentication failure: ";
            var_dump($array);
        }
    }
    
    
    // SEND SMS VIA TWILIO API
    function send_sms_via_twilio($message = '' , $reciever_phone = '') {
        
        // LOAD TWILIO LIBRARY
        require_once(APPPATH . 'libraries/twilio_library/Twilio.php');


        $account_sid    = $this->db->get_where('settings', array('type' => 'twilio_account_sid'))->row()->description;
        $auth_token     = $this->db->get_where('settings', array('type' => 'twilio_auth_token'))->row()->description;
        $client         = new Services_Twilio($account_sid, $auth_token); 

        $client->account->messages->create(array( 
            'To'        => $reciever_phone, 
            'From'      => $this->db->get_where('settings', array('type' => 'twilio_sender_phone_number'))->row()->description,
            'Body'      => $message   
        ));

    }
}